# Instalación Rapida

Para transformar tu Raspberry Pi 3 en un backend de libreCast es suficiente grabar una tarjeta SD 
con el sistema completo pre-instalado y listo para funcionar.

Los siguientes pasos explican como preparar y grabar tu tarjeta SD, no te olvides que esta operacion eliminara'
completamente cualquier informacion contenida previamente en la tarjeta.

## 1 Empezar fdisk para particionar la tarjeta SD:

```
fdisk /dev/sdX
```

Donde `sdX` es el nombre asignado por el sistema a la tarjeta SD (ej. sdf). Para verificar el nombre asignado al dispositivo se puede ejecutar el comando `lsblk`.

## 2 En el prompt de fdisk, borrar las particiones antiguas y crea una nueva:

   a. Digita **o**. Esto borrara' todas la particiones del disco.

   b. Digita **p** para ver la lista de particiones. No debería estar ninguna partición.

   c. Digita **n**, después **p** para partición primaria, **1** para la primera partición en el disco, presiona ENTER para aceptar el primer sector, después digite **+100M** para el ultimo sector.

   d. Digita **t**, después **c** para ajustar la primera partición con el tipo W95 FAT32 (LBA).

   e. Digita **n**, después **p** para partición primaria, **2** para la segunda partición del disco, después presione ENTER dos veces para aceptar el primero sector y el ultimo.

   f. Escribir la tabla de partición y salir digitando **w**.

## 3 Crea y monta el filesystem FAT:

```
mkfs.vfat /dev/sdX1
mkdir boot
mount /dev/sdX1 boot
```

## 4 Crea y monta el filesystem ext4:

```
mkfs.ext4 /dev/sdX2
mkdir root
mount /dev/sdX2 root
```

## 5 Descarga y extrae el filesystem de root (como root, no via sudo)

```
wget -O libreCast.tar.gz https://intranet.agetic.gob.bo/nube/index.php/s/FKEecmXRs7zlht1/download
bsdtar -xpf libreCast.tar.gz -C root
sync
```

## 6 Mueve los archivos de boot a la primera partición:

```
mv root/boot/* boot
```

## 7 Desmonta las dos particiones:

```
umount boot root
```

## 8 Introduce la tarjeta SD en el Raspberry Pi, conecta el cable ethernet, conecta el cable HDMI, y conecta alimentador de 5V.

## 9 Entra vía SSH a la IP del Raspberry Pi asignada por tu router.

   * Ingresa con el usuario por defecto **alarm** y contraseña **alarm**
   * La contraseña por defecto de root es **root**.

## 10 Pasos siguientes

Se recomienda cambiar las contraseñas de **alarm** y **root** después del primer arranque.



Una vez instalado libreCast es posible editar el nombre, el canal y otros parametros del Access Point editando el archivo `/etc/hostapd/hostapd.conf`



Para mayores informaciones puedes consultar la [wiki](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3) de ArchlinuxArm.
