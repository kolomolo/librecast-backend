on the server
base installation
pulseaudio 
alsa
omxplayer
fbida
systemd:
  iptables -> setup rules for internet sharing
  hostapd -> setup wifi AP
  networkd -> eth0 static ip
           -> wlan0 static ip

edit the file
```
/etc/sysctl.d/30-ipforward.conf

net.ipv4.ip_forward=1
net.ipv6.conf.default.forwarding=1
net.ipv6.conf.all.forwarding=1
```

setup config.txt accordingly

gstremer is not needed on the server (yey !)
