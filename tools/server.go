package main

import (
  "net/http"
  "log"
  "os/exec"
)

func GetUsage(w http.ResponseWriter, req *http.Request) {
  w.Header().Set("Content-Type", "text/plain")
  w.Write([]byte("Raspberry pi3 receiver.\nUsage:\nplayer/start\nplayer/stop"))
}

func StopPlayers() {
  exec.Command("pkill","-15","omxplayer.bin").Run()
  exec.Command("pkill","-15","gst-launch-1.0").Run()
}

func PlayerStart(w http.ResponseWriter, req *http.Request) {
  StopPlayers()
  w.Header().Set("Content-Type", "application/json")

  if oir := PlayerIsRunning(); oir == false {
    cmd := exec.Command("sh","playerstarter")
    cmd.Run()
    cmd.Wait()
    w.Write([]byte("{\"msg\":\"Player iniciado\"}"))
  } else {
    http.Error(w, "{\"msg\":\"Player fue ya ejecutado\"}",http.StatusInternalServerError)
  }
}

func PlayerIsRunning() (isrunning bool) {
  cmd := exec.Command("pidof","omxplayer.bin")
  if err:= cmd.Run(); err != nil {
    return false
  }
  return true
}

func PlayerStop(w http.ResponseWriter, req *http.Request) {
  w.Header().Set("Content-Type", "application/json")
  if oir := PlayerIsRunning(); oir == true {
    exec.Command("pkill","-15","omxplayer.bin").Run()
    exec.Command("pkill","-15","gst-launch-1.0").Run()
    w.Write([]byte("{\"msg\":\"Player Detenido\"}"))
  } else {
    http.Error(w, "{\"msg\":\"Ningun Player detectado\"}",http.StatusInternalServerError)
  }
}

func main() {
  http.HandleFunc("/", GetUsage)
  http.HandleFunc("/player/start", PlayerStart)
  http.HandleFunc("/player/stop", PlayerStop)
  err := http.ListenAndServeTLS(":5443", "tls/server.crt", "tls/server.key", nil)
  if err != nil {
      log.Fatal("ListenAndServe: ", err)
  }
}

