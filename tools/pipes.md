WebCam:
video /dev/video0
audio hw:2,0


# discover input and output PulseAudio devices and names
pactl list


# Play sound from the WebCam:

gst-launch-1.0 pulsesrc device=alsa_input.usb-046d_0825_1975DD80-02.analog-mono ! audioconvert ! alsasink


# Capturing Alsa Audio
gst-launch-1.0 alsasrc device="hw:2,0"  ! "audio/x-raw,channels=1,rate=48000,depth=16" ! audioconvert ! alsasink


# Sending video through udp



# Playing video from udp
gst-launch-1.0 udpsrc port=1234 ! application/x-rtp,encoding-name=JPEG,payload=26 ! rtpjpegdepay ! jpegdec ! autovideosink


# Send h264
gst-launch-1.0 -v v4l2src ! 'video/x-raw, width=800, height=600, framerate=20/1' ! videoconvert ! x264enc pass=qual quantizer=20 tune=zerolatency ! rtph264pay ! udpsink host=192.168.12.34 port=1234

# Receive
gst-launch-1.0 udpsrc port=1234 ! "application/x-rtp,width=800,height=600, payload=96" ! rtph264depay ! avdec_h264 ! videoconvert  ! autovideosink 


# View the screen uncompressed
gst-launch-1.0 -v ximagesrc use-damage=false show-pointer=0 startx=1920 starty=0 endx=3839 endy=1079 ! videoconvert ! videoscale ! video/x-raw,width=1919,height=1079 ! autovideosink


# Sending uncompressed video from v4l2src (hdmi or webcam) via udpsink 
  sender:
gst-lunch-1.0 -v v4l2src ! rtpvrawpay ! udpsink host=127.0.0.1 port=5000

  receiver:
gst-launch-1.0 udpsrc port=5000 caps = "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)RAW, sampling=(string)YCbCr-4:2:0, depth=(string)8, width=(string)1280, height=(string)960, colorimetry=(string)BT709-2, payload=(int)96, ssrc=(uint)3950430095, timestamp-offset=(uint)1453907103, seqnum-offset=(uint)31684" ! rtpvrawdepay ! videoconvert ! queue ! xvimagesink sync=false

# Sending uncompressed video from X screen via udpsink with chunk fixing

  sender:

  ```
  gst-launch-1.0 -v ximagesrc use-damage=false startx=1920 starty=0 endx=3839 endy=1079 ! videoconvert ! videoscale ! video/x-raw,width=1280,height=720 ! rtpvrawpay chunks-per-frame=25 ! udpsink host=127.0.0.1 port=1234
  ```

  receiver:
  ```
gst-launch-1.0 udpsrc port=1234 caps = "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)RAW, sampling=(string)RGB, depth=(string)8, width=(string)1280, height=(string)720, colorimetry=(string)SMPTE240M, payload=(int)96, ssrc=(uint)2491997159, timestamp-offset=(uint)1290939117, seqnum-offset=(uint)
7818" ! rtpvrawdepay ! videoconvert ! queue ! autovideosink sync=false
  ```
