# Instalación

## Lista de Materiales

Por cada pantalla / proyector que se quiera habilitar:

* 1 x Raspberry Pi 3
* 1 x Cable HDMI
* 1 x Tarjeta SD Categoría 10 minimo 4GB
* 1 x Fuente de alimentación de 5V @ 2.5A

Para compartir el acceso a internet:

1 x Puerto  de Red con acceso a Internet vía
1 x Cable Ethernet

## Instalación del Sistema Operativo 

El servicio libreCast se basa sobre sistema operativo Linux.
El sistema ha sido verificado con la distribución ArchlinuxARM para ARMv8.

Para la instalación de dicho sistema en tu RPi3 es suficiente seguir los pasos de la guía oficial a la pagina:

[Arch Linux ARM para Raspberry Pi 3](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3)

## Instalación de las dependencias

Sera' necesario instalar `hostapd` para el access point wifi, `omxplayer` para reproducir vídeos, `alsa-utils` para el audio, `gstreamer` para reproducir sonido y `fbida` para visualizar el splash screen.

```sh
# pacman -S hostapd omxplayer alsa-utils fbida gst-plugins-base gst-plugins-good gstreamer imagemagick ttf-liberation
```

## Permisos de usuario

Para otorgar permisos de consumo de recursos audio y vídeo al usuario `alarm`:

```sh
# usermod -aG audio,video alarm
```

## Configuraciones de boot

Modificar el archivo `/boot/config.txt` con el siguiente contenido:

```
hdmi_group=1
hdmi_mode=5
dtparam=audio=on                               
disable_overscan=1                             
hdmi_drive=2
audio_pwm_mode=2                               
gpu_mem=256 avoid_warnings=2 dtoverlay=vc4-kms-v3d
initramfs initramfs-linux.img followkernel
```

Es posible que la configuración no funcione con tu modelo especifico de pantalla,
eventualmente será necesario ajustar el grupo de pantalla mediante el parámetro `hdmi_group`
y el modo mediante el parámetro `hdmi_mode`.

Para mayores informaciones sobre como configurar el Raspberry para tu pantalla se recomienda el manual:

[Raspberry Pi Documentación](https://www.raspberrypi.org/documentation/configuration/config-txt/video.md) 

## Configuración de splash screen

El splash screen se configura habilitando como servicio el script `info2fb`.

El script info2fb crea dinámicamente una imagen de fondo con las informaciones de configuración del dispositivos.

El splashscreen se compone de 2 imagenes: titulos y fondo.

Copiar la imagen `tools/titles.png` de este repositorio al raspberry en `/home/alarm/titles.png`.

```sh
scp tools/titles.png alarm@192.168.ab.cd:~/
```

La imagen `splash.png` seria el fondo de la pantalla cuando el dispositivo no esta' transmitiendo.
Copiar la imagen `tools/splash.png` de este repositorio al raspberry en `/home/alarm/splash.png`.

Es posible adoptar una imagen diferente, en este caso se recomienda adoptar una imagen en formato PNG y resolución 1920x1080 px lo cual es la 
resolución máxima de la salida vídeo del raspberry pi 3.

```sh
scp tools/splash.png alarm@192.168.ab.cd:~/
```

Crear el archivo `/home/alarm/info2fb` con el siguiente contenido:

```
#!/bin/env bash

SPLASH=/var/tmp/splash.png
IP_ADDR=$(ip addr show dev eth0 | grep -Po 'inet \K[\d.]+')
ROOM_NAME=$(grep -Po '^ssid=\K[^ ]+' /etc/hostapd/hostapd.conf)
convert /home/alarm/splash.png -font Inconsolata -pointsize 50 -draw "fill #999999 text 30,1000 '$ROOM_NAME' " -pointsize 30 -draw "fill #999999 text 1600,1000 '$IP_ADDR' " $SPLASH
fbi -d /dev/fb0 -noverbose $SPLASH
```

Crea el archivo `/etc/systemd/system/splashscreen.service`
con el siguiente contenido:

```
[Unit]
Description=Splash Screen
Wants=network-online.target
After=network-online.target

[Service]
User=root
Group=root
ExecStart=/usr/bin/bash /home/alarm/info2fb
StandardInput=tty
StandardOutput=tty

[Install]
WantedBy=multi-user.target
```



Habilitar el servicio de splashcreen

```sh
# systemctl enable splashscreen
```
También hay que deshabilitar la terminal tty1 para evitar el prompt de login.

```sh
# systemctl disable getty@tty1
```

## Configuración del Access Point WiFi

Crear el archivo `/etc/hostapd/hostapd.conf` con el contenido:

```
interface=wlan0
driver=nl80211
ssid=EL_NOMBRE_DEL_AP
wpa_passphrase=CONTRASENA
channel=11
hw_mode=g
ieee80211n=1
wmm_enabled=1
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP CCMP
ctrl_interface=/var/run/hostapd
ctrl_interface_group=wheel
```

También es necesario crear y editar el archivo de configuración de red `/etc/systemd/network/wlan0.network` con el siguiente contenido:

```
[Match]
Name=wlan0

[Network]
Address=192.168.12.1/24
```

Habilitar el servicio `hostapd.service` 

## Compartir internet

Habilitar el forwarding de paquetes creando y editando el archivo `/etc/sysctl.d/30-ipforward.conf` con el contenido:

```
net.ipv4.ip_forward=1
net.ipv6.conf.default.forwarding=1
net.ipv6.conf.all.forwarding=1
```

Configurar iptables  para compartir el internet editando el archivo `/etc/iptables/iptables.rules`

```
*filter
:INPUT ACCEPT [161:11144]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [84:7728]
-A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
-A FORWARD -i wlan0 -o eth0 -j ACCEPT
COMMIT

*nat
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:OUTPUT ACCEPT [1:76]
:POSTROUTING ACCEPT [0:0]
-A POSTROUTING -o eth0 -j MASQUERADE
COMMIT
```

y habilitar el servicio iptables.

Para mayores informaciones se recomienda la siguiente guía:

[Internet Sharing](https://wiki.archlinux.org/index.php/Internet_sharing)


## Compilar el servicio libreCast con GOLang

Tu maquina requiere un compilador GoLang

Por favor consultar la guía de instalación a la pagina:

[Go Lang](https://golang.org/)

En tu maquina, NO en el Raspberry Pi, clonar el proyecto `librecast-backend`:

```sh
git clone git@gitlab.agetic.gob.bo:agetic/librecast-backend.git
```

Entrar al proyecto y compilar el archivo `server.go` para ARMv7

```sh
cd librecast-backend
GOOS=linux GOARCH=arm GOARM=7 go build -ldflags="-s -w" -o librecast-armv7 server.go
```

Se creará un archivo ejecutable `librecast-armv7`

## Instalar el servicio libreCast en RPi3 


Copiar el archivo `librecast-armv7` desde tu maquina al filesystem del RPi3, en la carpeta `$HOME` del usuario `alarm`.

Se puede lograr la copia mediante scp
```sh
scp librecast-armv7 alarm@192.168.ab.cd:~/
```
donde `192.168.ab.cd` es la dirección IP del RPi3.

También copiar el archivo `playerstarter` en la `$HOME` del usuario `alarm`.

Crear el archivo `/etc/systemd/system/player.service` con el siguiente contenido:

```
[Unit]
Description=Librecast
After=network.target

[Service]
Type=simple
User=alarm
Group=alarm
ExecStart=/home/alarm/librecast-armv7
WorkingDirectory=/home/alarm
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

Crear la carpeta `/home/alarm/tls` con clave y  certificado: `server.key` y `server.crt`.

Habilitar el servicio :

```sh
# systemctl enable player.service
```

Abrir y editar el archivo `/etc/dnsmasq.conf` de acuerdo a [esta guia de dnsmasq](https://wiki.archlinux.org/index.php/Dnsmasq#DHCP_server_setup) .

## Instalar y configurar DNSmasq para DHCP

como `root` instalar el paquete `dnsmasq`

```sh
# pacman -S dnsmasq
```

## Configurar el RPi3 como  read-only

Esta parte es opcional y permite proteger la tarjeta SD.
 
Referirse a la guía:

[read only ](https://gist.github.com/yeokm1/8b0ffc03e622ce011010)
