# libreCast

El servicio de backend de libreCast para Raspberry Pi 3.

* [Instalación Rápida](RECOVERY.md)

* [Guía de instalación](INSTALL.md)

## Cliente para linux

Para el cliente libreCast por favor referirse al repositorio dedicado:

[cliente libreCast](https://gitlab.agetic.gob.bo/agetic/librecast)
